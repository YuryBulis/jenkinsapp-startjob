package com.bulis.config;

import com.cdancy.jenkins.rest.JenkinsClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public JenkinsClient jenkinsClient(){
        return JenkinsClient.builder()
                .endPoint("http://builder-ci.storksoft.by")
                .token("oBn5mhkw4W")
                .credentials("dreamnet:114cedaf117e3d1ed5f1f406641dc3c4c1")
                .build();
    }
}

package com.bulis.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Build {

    private Integer number;
    private Integer queueId;
    private String result;
    @JsonIgnore
    private String _class;

}

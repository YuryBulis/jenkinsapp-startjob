package com.bulis.controller;

import com.bulis.model.Build;
import com.cdancy.jenkins.rest.JenkinsClient;
import com.cdancy.jenkins.rest.domain.common.IntegerResponse;
import com.cdancy.jenkins.rest.domain.job.BuildInfo;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.common.collect.Lists;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/job")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class JobController {

    private static final String JENKINS_URL_WITH_TOKEN = "http://dreamnet:114cedaf117e3d1ed5f1f406641dc3c4c1@builder-ci.storksoft.by/job/TEST/buildWithParameters?token=oBn5mhkw4W";
    private static final String JENKINS_URL = "http://builder-ci.storksoft.by";
    private static final String JOB_NAME = "TEST";
    private final RestTemplate restTemplate;
    private final JenkinsClient jenkinsClient;

    @ApiOperation(value = "Start job with params (rest template)")
    @PostMapping("/rest/withParams/{param}")
    public void startJobWithParamsRest(@PathVariable String param) {

        restTemplate.postForEntity(JENKINS_URL_WITH_TOKEN + "&TEST=" + "{param}", getHeader(), null, param);
    }

    @ApiOperation(value = "Start job with params")
    @PostMapping("/withParams/{param}")
    public void startJobWithParams(@PathVariable String param) {

        Map<String, List<String>> params = new HashMap<>();
        params.put("TEST", Lists.newArrayList(param));
        IntegerResponse integerResponse = jenkinsClient.api().jobsApi().buildWithParameters(null, JOB_NAME, params);
    }

    @ApiOperation(value = "Get buildNumber")
    @GetMapping(value = "/build/{queueNumber}")
    public Build getBuildNumber(@PathVariable Integer queueNumber) throws IOException {

        String buildXml = restTemplate.postForEntity(JENKINS_URL + "/job/TEST/api/xml?tree=builds[number,result,queueId]&xpath=//build[queueId={queueNumber}]", getHeader(), String.class, queueNumber).getBody();
        XmlMapper xmlMapper = new XmlMapper();
        return xmlMapper.readValue(buildXml, Build.class);

    }

    @ApiOperation(value = "Get build")
    @GetMapping(value = "/{buildNumber}")
    public BuildInfo getBuild(@PathVariable Integer buildNumber) {

        return jenkinsClient.api().jobsApi().buildInfo(null, JOB_NAME, buildNumber);
    }

    private HttpEntity<String> getHeader(){
        HttpHeaders headers = new HttpHeaders();
        headers.setBasicAuth("dreamnet", "114cedaf117e3d1ed5f1f406641dc3c4c1");
        return new HttpEntity<>(null, headers);
    }
}
